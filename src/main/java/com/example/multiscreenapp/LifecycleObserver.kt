package com.example.multiscreenapp

import android.util.Log
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner

class LifecycleObserver : DefaultLifecycleObserver {
    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        Log.i(tag, "onCreate callback was triggered in ${owner.javaClass}")
    }

    override fun onStart(owner: LifecycleOwner) {
        super.onStart(owner)
        Log.i(tag, "onStart callback was triggered in ${owner.javaClass}")
    }

    override fun onResume(owner: LifecycleOwner) {
        super.onResume(owner)
        Log.i(tag, "onResume callback was triggered in ${owner.javaClass}")
    }

    override fun onPause(owner: LifecycleOwner) {
        super.onPause(owner)
        Log.i(tag, "onPause callback was triggered in ${owner.javaClass}")
    }

    override fun onStop(owner: LifecycleOwner) {
        super.onStop(owner)
        Log.i(tag, "onStop callback was triggered in ${owner.javaClass}")
    }

    override fun onDestroy(owner: LifecycleOwner) {
        super.onDestroy(owner)
        Log.i(tag, "onDestroy callback was triggered in ${owner.javaClass}")
    }
}