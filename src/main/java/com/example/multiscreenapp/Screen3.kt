package com.example.multiscreenapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Screen3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_screen3)

        val goForward : Button = findViewById(R.id.go_forward)
        val goBack : Button = findViewById(R.id.go_back)

        goForward.setOnClickListener {startActivity(Intent(this, Screen4::class.java))}
        goBack.setOnClickListener{finish()}
        lifecycle.addObserver(LifecycleObserver())
    }
}